<div class="container-fluid-1280">
	<div class="row">

		<ul class="list-group list-unstyled col-xs-12 col-sm-3">
			<li><img src="${images_folder}/araba/logo-gazteria-neg.png" class="ifj-footer-logo" alt="Gazteria" /></li>
			<li>
				<a href="http://www.araba.eus" target="_blank" title="Arabako Foru Aldundia">
					<img src="${images_folder}/araba/araba-logo-neg.png" class="ifj-footer-logo" alt="Araba.eus" />
				</a>
			</li>
		</ul>
		
		<div class="list-group col-xs-12 col-sm-3">		
			<a href="<@liferay.language key="ifj.theme.nav.jovenes.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.nav.jovenes" /></a>
			<a href="<@liferay.language key="ifj.theme.nav.asociaciones.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.nav.asociaciones" /></a>
			<a href="<@liferay.language key="ifj.theme.nav.otros.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.nav.otros" /></a>
		</div>		
		<div class="list-group col-xs-12 col-sm-3">
			<a href="<@liferay.language key="ifj.theme.footer.accesibility.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.footer.accesibility" /></a>
			<a href="<@liferay.language key="ifj.theme.footer.aviso-legal.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.footer.aviso-legal" /></a>
			<a href="<@liferay.language key="ifj.theme.footer.cookies.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.footer.cookies" /></a>
			<a href="<@liferay.language key="ifj.theme.footer.mapa-web.url" />" class="list-group-item btn-sm"><@liferay.language key="ifj.theme.footer.mapa-web" /></a>
		</div>
				
		<ul class="list-group col-xs-12 col-sm-3">
			<li class="list-group-item btn-sm">
				<@liferay.language key="ifj.theme.footer.follow" />
				<div class="ifj-social-icons">
					<div class="btn-group">
						<a class="btn" href="https://www.facebook.com/infogaztea.araba" target="_blank"><img src="${images_folder}/araba/icon-facebook-neg.png" alt="<@liferay.language key="ifj.theme.footer.facebook" />" /></a>
						<a class="btn" href="https://twitter.com/infogaztea" target="_blank"><img src="${images_folder}/araba/icon-twitter-neg.png" alt="<@liferay.language key="ifj.theme.footer.twitter" />" /></a>
						<a class="btn" href="https://www.instagram.com/infogaztea" target="_blank"><img src="${images_folder}/araba/icon-instagram-neg.png" alt="<@liferay.language key="ifj.theme.footer.instagram" />" /></a>
						<a class="btn" href="https://vimeo.com/ifjaraba" target="_blank"><img src="${images_folder}/araba/icon-vimeo-neg.png" alt="<@liferay.language key="ifj.theme.footer.vimeo" />" /></a>
					</div>
				</div>
			</li>
			<li class="list-group-item"><a href="mailto:ifj@araba.eus"><@liferay.language key="ifj.theme.footer.email" /></a></li>
			<li class="list-group-item"><@liferay.language key="ifj.theme.footer.phone" /></li>
			<li class="list-group-item"><@liferay.language key="ifj.theme.footer.street" /></li>
			<li class="list-group-item"><@liferay.language key="ifj.theme.footer.location" /></li>
		</ul>
	</div>

	<div id="ifj_avisocookies">
		<div class="container-fluid-1280">
			<p><@liferay.language key="ifj.theme.footer.cookies.text" /></p>
			<div class="button-cookies">
				<button onclick="aceptarCookies()"><@liferay.language key="ifj.theme.footer.cookies.check" /></button>
			</div>
		</div>
	</div>
</div>