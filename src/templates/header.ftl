<div class="container-fluid no-padding">
	<nav class="navbar navbar-inverse ifj-navbar-home">
		<div class="navbar-header">
			<div class="container-fluid-1280">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand logo custom-logo" href="${site_default_url}" >
					<img alt="${logo_description}" height="90" src="${site_logo}" />
				</a>
			</div>
		</div>		
		<div class="collapse navbar-collapse" id="myNavbar">
			<div class="row ifj-navbar-top">
				<div class="container-fluid-1280 d-flex justify-content-end">
					<div class="nav navbar-nav">
						<div class="btn-group">
							<#include "${full_templates_path}/language.ftl" />
							<a href="<@liferay.language key='ifj.theme.header.faqs.url' />" class="btn btn-inline"><@liferay.language key='ifj.theme.header.faqs' /></a>								
							<a href="<@liferay.language key='ifj.theme.InfoGaztea.newsletter.url' />" class="btn btn-inline"><@liferay.language key='ifj.theme.header.newsletter' /></a>
						</div>
					</div>
				</div>
			</div>
			<#include "${full_templates_path}/navigation.ftl" />
		</div>
	</nav>	
</div>		