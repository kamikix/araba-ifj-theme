<#assign show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search")) />
<#assign the_title = languageUtil.get(locale, the_title, page.getName(locale)) />

<#assign availableLocales = languageUtil.getAvailableLocales(group_id) >

<#assign inicioCustomField = 'ifj-pagina-inicio' >
<#assign tituloPaginaCustomField = 'ifj-mostrar-titulo-pagina' >
<#assign caminoMigasCustomField = 'ifj-mostrar-camino-migas' >
<#assign bodyClassesCustomField = 'ifj-body-styles' >


<#assign isInicio = themeDisplay.getLayout().getExpandoBridge().getAttribute(inicioCustomField) >
<#assign showTitle = themeDisplay.getLayout().getExpandoBridge().getAttribute(tituloPaginaCustomField) >
<#assign showMigas = themeDisplay.getLayout().getExpandoBridge().getAttribute(caminoMigasCustomField) >
<#assign bodyClasses = themeDisplay.getLayout().getExpandoBridge().getAttribute(bodyClassesCustomField) >