<#--
<#assign localeLanguage = locale.getLanguage()?upper_case />

<a href="#" type="button" class="btn btn-inline selected">${localeLanguage} | </a>

<#list availableLocales as availableLocale>
	 
	<#assign languageURL = "/c/portal/update_language?p_l_id=${themeDisplay.getPlid()}&redirect=${themeDisplay.getURLCurrent()}&languageId=${localeUtil.toLanguageId(availableLocale)}" />
	
	<#assign localeLanguage = availableLocale.getLanguage()?upper_case />
	       
	<#if availableLocale != locale>
		
        <a href="${languageURL}" type="button" class="btn btn-inline">${localeLanguage}</a>
		
	</#if>
</#list>
-->

<#if "es_ES" == locale>
	<#assign languageURL = "/c/portal/update_language?p_l_id=${themeDisplay.getPlid()}&redirect=${themeDisplay.getURLCurrent()}&languageId=eu_ES" />
	<span class="btn btn-inline"><strong>ES | </strong><a href="${languageURL}">EU</a></span>
<#else>
	<#assign languageURL = "/c/portal/update_language?p_l_id=${themeDisplay.getPlid()}&redirect=${themeDisplay.getURLCurrent()}&languageId=es_ES" />
	<span class="btn btn-inline"><strong>EU | </strong><a href="${languageURL}">ES</a></span>
</#if>

