<div class="row ifj-navbar">
	<div class="container-fluid-1280">
		<!--PORTLET MENU PRINCIPAL / IBERMATICA -->
		<@liferay_portlet["runtime"] portletName="com_liferay_site_navigation_menu_web_portlet_SiteNavigationMenuPortlet_INSTANCE_WgBq4mM8niCQ"/>
		
		
		<!--
		<li class="dropdown ifj-nav-jovenes">
			<a href="<@liferay.language key="ifj.theme.nav.jovenes.url" />" class="dropdown-toggle" id="menu1" data-toggle="dropdown"><@liferay.language key="ifj.theme.nav.jovenes" /><span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
				<li class="hidden-lg hidden-md"><a href="<@liferay.language key="ifj.theme.nav.jovenes.url" />"><@liferay.language key="ifj.theme.nav.inicio" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.albergues.url" />"><@liferay.language key="ifj.theme.nav.jovenes.albergues" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.actividades.url" />"><@liferay.language key="ifj.theme.nav.jovenes.actividades" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.ayudas.url" />"><@liferay.language key="ifj.theme.nav.jovenes.ayudas" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.infogaztea.url" />"><@liferay.language key="ifj.theme.nav.jovenes.infogaztea" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.legislacion.url" />"><@liferay.language key="ifj.theme.nav.jovenes.legislacion" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.jovenes.documentos.url" />"><@liferay.language key="ifj.theme.nav.jovenes.documentos" /></a></li>
			</ul>
		</li>
		<li class="dropdown ifj-nav-asociaciones">
			<a href="<@liferay.language key="ifj.theme.nav.asociaciones.url" />" class="dropdown-toggle" id="menu2" data-toggle="dropdown"><@liferay.language key="ifj.theme.nav.asociaciones" /><span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
				<li class="hidden-lg hidden-md"><a href="<@liferay.language key="ifj.theme.nav.asociaciones.url" />"><@liferay.language key="ifj.theme.nav.inicio" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.asociaciones.albergues.url" />"><@liferay.language key="ifj.theme.nav.asociaciones.albergues" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.asociaciones.material.url" />"><@liferay.language key="ifj.theme.nav.asociaciones.material" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.asociaciones.tiempo.url" />"><@liferay.language key="ifj.theme.nav.asociaciones.tiempo" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.asociaciones.ayudas.url" />"><@liferay.language key="ifj.theme.nav.asociaciones.ayudas" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.asociaciones.legislacion.url" />"><@liferay.language key="ifj.theme.nav.asociaciones.legislacion" /></a></li>
			</ul>
		</li>
		<li class="dropdown ifj-nav-colectivos">
			<a href="<@liferay.language key="ifj.theme.nav.otros.url" />" class="dropdown-toggle" id="menu3" data-toggle="dropdown"><@liferay.language key="ifj.theme.nav.otros" /><span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="menu3">
				<li class="hidden-lg hidden-md"><a href="<@liferay.language key="ifj.theme.nav.otros.url" />"><@liferay.language key="ifj.theme.nav.inicio" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.otros.albergues.url" />"><@liferay.language key="ifj.theme.nav.otros.albergues" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.otros.programas.url" />"><@liferay.language key="ifj.theme.nav.otros.programas" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.otros.ayudas.url" />"><@liferay.language key="ifj.theme.nav.otros.ayudas" /></a></li>
				<li><a href="<@liferay.language key="ifj.theme.nav.otros.legislacion.url" />"><@liferay.language key="ifj.theme.nav.otros.legislacion" /></a></li>
			</ul>
		</li>
		-->
	</div>
</div>