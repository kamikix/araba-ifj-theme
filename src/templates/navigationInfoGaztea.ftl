<div class="row ifj-navbar ifj-navbar-section-gazteak">
	<div class="container-fluid-1280">
		<!--PORTLET MENU GAZTEA / IBERMATICA -->
		<@liferay_portlet["runtime"] portletName="com_liferay_site_navigation_menu_web_portlet_SiteNavigationMenuPortlet_INSTANCE_JM6MWFbmr2Ps"/>
		
		<!--
		<#list nav_items as nav_item>
	        <#assign
	            nav_item_attr_has_popup = ""
	            nav_item_attr_selected = ""
	            nav_item_css_class = ""
	            nav_item_layout = nav_item.getLayout()
	        />
	
	        <#if nav_item.isSelected()>
	            <#assign
	                nav_item_attr_has_popup = "aria-haspopup='true'"
	                nav_item_attr_selected = "aria-selected='true'"
	                nav_item_css_class = "ifj-select"
	            />
	        </#if>
           
			<ul class="nav navbar-nav" aria-label="<@liferay.language key="site-pages" />" role="menubar">         
				<li ${nav_item_attr_selected} class="ifj-section ${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
					<a aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span></a>
				</li>
			</ul>
				
			<#if nav_item.hasChildren()>
	            <ul class="nav navbar-nav navbar-right">
	                <#list nav_item.getChildren() as nav_child>
	                    <#assign
	                        nav_child_attr_selected = ""
	                        nav_child_css_class = ""
	                    />
	
	                    <#if nav_child.isSelected()>
	                        <#assign
	                            nav_child_attr_selected = "aria-selected='true'"
	                            nav_child_css_class = "ifj-select"
	                        />
	                    </#if>
	
	                    <li ${nav_child_attr_selected} class="btn-sm" id="layout_${nav_child.getLayoutId()}" role="presentation">
	                        <a aria-labelledby="layout_${nav_child.getLayoutId()}" class="${nav_child_css_class}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
	                    </li> 
	                </#list>  
	            </ul>        
        	</#if>
       	</#list>
		-->
	</div>
</div>
