<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${site_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<!--[if IE]>
		<![if lte IE 11]>
		   <link rel="stylesheet" type="text/css" href="/o/araba-ifj-theme/css/fonts/muli/muli-ie.css" />
		<![endif]>
    <![endif]-->
      
	<!--[if !IE]><! -->
		<link rel="stylesheet" type="text/css" href="/o/araba-ifj-theme/css/fonts/muli/muli.css" />
	<!--<![endif]-->
    
	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class} ${bodyClasses}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div id="wrapper">

	<#if isInicio>
		<!-- Imagen de la cabecera de inicio -->
		<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
		<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", "group_id") /> 
		
		<!-- Id webContent Entorno desarrollo Zylk -->
		<#assign webContentId = "69411" />
		
		<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", "webContentId") />

		<@liferay_portlet["runtime"]
		defaultPreferences="${freeMarkerPortletPreferences}"
		portletProviderAction=portletProviderAction.VIEW
		portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />
		${freeMarkerPortletPreferences.reset()}
		
		<!-- Plantilla - Header general - start (a partir de aqui se puede modificar cualquiewr cosa) -->
		<header class="container-fluid" id="banner" role="banner">
			<#include "${full_templates_path}/header.ftl" />
		</header>
		<!-- Plantilla - Header general - end (hasta aqui se puede modificar cualquiewr cosa) -->
	<#else>
		<!-- Plantilla - Header infogaztea - start (a partir de aqui se puede modificar cualquiewr cosa) -->
		<header class="container-fluid" id="banner" role="banner">
			<#include "${full_templates_path}/headerInfoGaztea.ftl" />
		</header>
		<!-- Plantilla - Header infogaztea - end (hasta aqui se puede modificar cualquiewr cosa) -->
	</#if>
	
	<section class="container-fluid" id="content">
		
		<#if showMigas>
			<nav id="breadcrumbs">
				<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone")>
				<@liferay.breadcrumbs default_preferences="${freeMarkerPortletPreferences}" />
				<#assign VOID = freeMarkerPortletPreferences.reset()>
			</nav>
		</#if>
		
		<#if showTitle>
			<!-- Plantilla - Titulo - start (a partir de aqui se puede modificar cualquiewr cosa) -->
			<h1 class="ifj-main-title">${the_title}</h1>
			<!-- Plantilla - Titulo - end (hasta aqui se puede modificar cualquiewr cosa) -->
		</#if>

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
		
	</section>

	<!-- Plantilla - Footer - start (a partir de aqui se puede modificar cualquiewr cosa) -->
	<footer class="container-fluid ifj-footer" id="footer" role="contentinfo">
		<#include "${full_templates_path}/footer.ftl" />
	</footer>
	<!-- Plantilla - Footer - end (hasta aqui se puede modificar cualquiewr cosa) -->
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>